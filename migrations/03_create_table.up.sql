CREATE TABLE shippers(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "phone_number" VARCHAR,
    "status" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);