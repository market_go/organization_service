--Organization Service
CREATE TABLE branches(
    "id" UUID PRIMARY KEY,
    "branch_id" VARCHAR NOT NULL UNIQUE,
    "name" VARCHAR,
    "address" VARCHAR,
    "phone_number" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE magazines(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "branch" UUID REFERENCES branches("id"),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

CREATE TABLE staffs(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "surname" VARCHAR,
    "phone_number" VARCHAR,
    "login" VARCHAR NOT NULL,
    "password" VARCHAR NOT NULL,
    "type" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);