CREATE TABLE shippers(
    "id" UUID PRIMARY KEY,
    "name" VARCHAR,
    "status" VARCHAR,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);