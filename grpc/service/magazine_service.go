package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/organization_service/config"
	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/grpc/client"
	"gitlab.com/market_go/organization_service/pkg/logger"
	"gitlab.com/market_go/organization_service/storage"
)

type MagazineService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedMagazineServiceServer
}

func NewMagazineService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *MagazineService {
	return &MagazineService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *MagazineService) Create(ctx context.Context, req *organization_service.MagazineCreate) (*organization_service.Magazine, error) {
	u.log.Info("====== Magazine Create ======", logger.Any("req", req))

	resp, err := u.strg.Magazine().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Magazine: u.strg.Magazine().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MagazineService) GetById(ctx context.Context, req *organization_service.MagazinePrimaryKey) (*organization_service.Magazine, error) {
	u.log.Info("====== Magazine Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Magazine().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Magazine Get By ID: u.strg.Magazine().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MagazineService) GetList(ctx context.Context, req *organization_service.MagazineGetListRequest) (*organization_service.MagazineGetListResponse, error) {
	u.log.Info("====== Magazine Get List ======", logger.Any("req", req))

	resp, err := u.strg.Magazine().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Magazine Get List: u.strg.Magazine().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MagazineService) Update(ctx context.Context, req *organization_service.MagazineUpdate) (*organization_service.Magazine, error) {
	u.log.Info("====== Magazine Update ======", logger.Any("req", req))

	resp, err := u.strg.Magazine().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Magazine Update: u.strg.Magazine().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *MagazineService) Delete(ctx context.Context, req *organization_service.MagazinePrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Magazine Delete ======", logger.Any("req", req))

	err := u.strg.Magazine().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Magazine Delete: u.strg.Magazine().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
