package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/market_go/organization_service/config"
	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/grpc/client"
	"gitlab.com/market_go/organization_service/pkg/logger"
	"gitlab.com/market_go/organization_service/storage"
)

type ShipperService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedShipperServiceServer
}

func NewShipperService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShipperService {
	return &ShipperService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ShipperService) Create(ctx context.Context, req *organization_service.ShipperCreate) (*organization_service.Shipper, error) {
	u.log.Info("====== Shipper Create ======", logger.Any("req", req))

	resp, err := u.strg.Shipper().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Shipper: u.strg.Shipper().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShipperService) GetById(ctx context.Context, req *organization_service.ShipperPrimaryKey) (*organization_service.Shipper, error) {
	u.log.Info("====== Shipper Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Shipper().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Shipper Get By ID: u.strg.Shipper().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShipperService) GetList(ctx context.Context, req *organization_service.ShipperGetListRequest) (*organization_service.ShipperGetListResponse, error) {
	u.log.Info("====== Shipper Get List ======", logger.Any("req", req))

	resp, err := u.strg.Shipper().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Shipper Get List: u.strg.Shipper().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShipperService) Update(ctx context.Context, req *organization_service.ShipperUpdate) (*organization_service.Shipper, error) {
	u.log.Info("====== Shipper Update ======", logger.Any("req", req))

	resp, err := u.strg.Shipper().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Shipper Update: u.strg.Shipper().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ShipperService) Delete(ctx context.Context, req *organization_service.ShipperPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Shipper Delete ======", logger.Any("req", req))

	err := u.strg.Shipper().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Shipper Delete: u.strg.Shipper().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
