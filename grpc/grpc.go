package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/market_go/organization_service/config"
	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/grpc/client"
	"gitlab.com/market_go/organization_service/grpc/service"
	"gitlab.com/market_go/organization_service/pkg/logger"
	"gitlab.com/market_go/organization_service/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	organization_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	organization_service.RegisterMagazineServiceServer(grpcServer, service.NewMagazineService(cfg, log, strg, srvc))
	organization_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))
	organization_service.RegisterShipperServiceServer(grpcServer, service.NewShipperService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
