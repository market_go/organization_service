package storage

import (
	"context"

	"gitlab.com/market_go/organization_service/genproto/organization_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	Magazine() MagazineRepoI
	Staff() StaffRepoI
	Shipper() ShipperRepoI
}

type BranchRepoI interface {
	Create(context.Context, *organization_service.BranchCreate) (*organization_service.Branch, error)
	GetByID(context.Context, *organization_service.BranchPrimaryKey) (*organization_service.Branch, error)
	GetList(context.Context, *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error)
	Update(context.Context, *organization_service.BranchUpdate) (*organization_service.Branch, error)
	Delete(context.Context, *organization_service.BranchPrimaryKey) error
}

type MagazineRepoI interface {
	Create(context.Context, *organization_service.MagazineCreate) (*organization_service.Magazine, error)
	GetByID(context.Context, *organization_service.MagazinePrimaryKey) (*organization_service.Magazine, error)
	GetList(context.Context, *organization_service.MagazineGetListRequest) (*organization_service.MagazineGetListResponse, error)
	Update(context.Context, *organization_service.MagazineUpdate) (*organization_service.Magazine, error)
	Delete(context.Context, *organization_service.MagazinePrimaryKey) error
}

type StaffRepoI interface {
	Create(context.Context, *organization_service.StaffCreate) (*organization_service.Staff, error)
	GetByID(context.Context, *organization_service.StaffPrimaryKey) (*organization_service.Staff, error)
	GetList(context.Context, *organization_service.StaffGetListRequest) (*organization_service.StaffGetListResponse, error)
	Update(context.Context, *organization_service.StaffUpdate) (*organization_service.Staff, error)
	Delete(context.Context, *organization_service.StaffPrimaryKey) error
}

type ShipperRepoI interface {
	Create(context.Context, *organization_service.ShipperCreate) (*organization_service.Shipper, error)
	GetByID(context.Context, *organization_service.ShipperPrimaryKey) (*organization_service.Shipper, error)
	GetList(context.Context, *organization_service.ShipperGetListRequest) (*organization_service.ShipperGetListResponse, error)
	Update(context.Context, *organization_service.ShipperUpdate) (*organization_service.Shipper, error)
	Delete(context.Context, *organization_service.ShipperPrimaryKey) error
}
