package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/pkg/helper"
)

type StaffRepo struct {
	db *pgxpool.Pool
}

func NewStaffRepo(db *pgxpool.Pool) *StaffRepo {
	return &StaffRepo{
		db: db,
	}
}

func (r *StaffRepo) Create(ctx context.Context, req *organization_service.StaffCreate) (*organization_service.Staff, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	password, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	query = `
		INSERT INTO staffs(id, name, surname, phone_number, login, password, type, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7, NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Surname,
		req.PhoneNumber,
		req.Login,
		string(password),
		req.Type,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Staff{
		Id:          id,
		Surname:     req.Surname,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		Type:        req.Type,
	}, nil
}

func (r *StaffRepo) GetByID(ctx context.Context, req *organization_service.StaffPrimaryKey) (*organization_service.Staff, error) {

	var (
		query    string
		where    string
		argument string

		Id          sql.NullString
		Name        sql.NullString
		Surname     sql.NullString
		PhoneNumber sql.NullString
		Login       sql.NullString
		Password    sql.NullString
		Type        sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)
	if len(req.Id) > 0 {
		where = " WHERE id = $1"
		argument = req.Id
	} else if len(req.Login) > 0 {
		where = " WHERE login = $1"
		argument = req.Login
	}

	query = `
		SELECT
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			type,
			created_at,
			updated_at
		FROM staffs
	`
	query += where
	err := r.db.QueryRow(ctx, query, argument).Scan(
		&Id,
		&Name,
		&Surname,
		&PhoneNumber,
		&Login,
		&Password,
		&Type,
		&CreatedAt,
		&UpdatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.Staff{
		Id:          Id.String,
		Name:        Name.String,
		Surname:     Surname.String,
		PhoneNumber: PhoneNumber.String,
		Login:       Login.String,
		Password:    Password.String,
		Type:        Type.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}, nil
}

func (r *StaffRepo) GetList(ctx context.Context, req *organization_service.StaffGetListRequest) (*organization_service.StaffGetListResponse, error) {

	var (
		resp   = &organization_service.StaffGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			surname,
			phone_number,
			login,
			password,
			type,
			created_at,
			updated_at	
		FROM staffs
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%' or surname ILIKE '%' || '` + req.Search + `' || '%' or phone_numn ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			Name        sql.NullString
			Surname     sql.NullString
			PhoneNumber sql.NullString
			Login       sql.NullString
			Password    sql.NullString
			Type        sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&Name,
			&Surname,
			&PhoneNumber,
			&Login,
			&Password,
			&Type,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Staffes = append(resp.Staffes, &organization_service.Staff{
			Id:          Id.String,
			Name:        Name.String,
			Surname:     Surname.String,
			PhoneNumber: PhoneNumber.String,
			Login:       Login.String,
			Password:    Password.String,
			Type:        Type.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *StaffRepo) Update(ctx context.Context, req *organization_service.StaffUpdate) (*organization_service.Staff, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			staffs
		SET
			name = :name,	
			surname = :surname,	
			phone_number = :phone_number,	
			login = :login,	
			password = :password,	
			type = :type,	
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"surname":      req.Surname,
		"phone_number": req.PhoneNumber,
		"login":        req.Login,
		"password":     req.Password,
		"type":         req.Type,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Staff{
		Id:          req.Id,
		Name:        req.Name,
		Surname:     req.Surname,
		PhoneNumber: req.PhoneNumber,
		Login:       req.Login,
		Password:    req.Password,
		Type:        req.Type,
	}, nil
}

func (r *StaffRepo) Delete(ctx context.Context, req *organization_service.StaffPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM staffs WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
