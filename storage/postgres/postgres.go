package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/organization_service/config"
	"gitlab.com/market_go/organization_service/storage"
)

type Store struct {
	db       *pgxpool.Pool
	branch   *BranchRepo
	magazine *MagazineRepo
	staff    *StaffRepo
	shipper  *ShipperRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewBranchRepo(s.db)
	}

	return s.branch
}

func (s *Store) Magazine() storage.MagazineRepoI {
	if s.magazine == nil {
		s.magazine = NewMagazineRepo(s.db)
	}

	return s.magazine
}

func (s *Store) Staff() storage.StaffRepoI {
	if s.staff == nil {
		s.staff = NewStaffRepo(s.db)
	}

	return s.staff
}

func (s *Store) Shipper() storage.ShipperRepoI {
	if s.shipper == nil {
		s.shipper = NewShipperRepo(s.db)
	}

	return s.shipper
}
