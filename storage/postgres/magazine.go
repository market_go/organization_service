package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/pkg/helper"
)

type MagazineRepo struct {
	db *pgxpool.Pool
}

func NewMagazineRepo(db *pgxpool.Pool) *MagazineRepo {
	return &MagazineRepo{
		db: db,
	}
}

func (r *MagazineRepo) Create(ctx context.Context, req *organization_service.MagazineCreate) (*organization_service.Magazine, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO magazines(id, branch, name, updated_at)
		VALUES ($1, $2, $3, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Branch,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Magazine{
		Id:     id,
		Branch: req.Branch,
		Name:   req.Name,
	}, nil
}

func (r *MagazineRepo) GetByID(ctx context.Context, req *organization_service.MagazinePrimaryKey) (*organization_service.Magazine, error) {

	var (
		query string

		Id        sql.NullString
		BranchId  sql.NullString
		Name      sql.NullString
		CreatedAt sql.NullString
		UpdatedAt sql.NullString
	)

	query = `
		SELECT
			id,
			branch,
			name,
			created_at,
			updated_at
		FROM magazines
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&BranchId,
		&Name,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Magazine{
		Id:        Id.String,
		Branch:    BranchId.String,
		Name:      Name.String,
		CreatedAt: CreatedAt.String,
		UpdatedAt: UpdatedAt.String,
	}, nil
}

func (r *MagazineRepo) GetList(ctx context.Context, req *organization_service.MagazineGetListRequest) (*organization_service.MagazineGetListResponse, error) {

	var (
		resp   = &organization_service.MagazineGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch,
			name,
			created_at,
			updated_at	
		FROM magazines
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id        sql.NullString
			BranchId  sql.NullString
			Name      sql.NullString
			CreatedAt sql.NullString
			UpdatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&Name,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Magazinees = append(resp.Magazinees, &organization_service.Magazine{
			Id:        Id.String,
			Branch:    BranchId.String,
			Name:      Name.String,
			CreatedAt: CreatedAt.String,
			UpdatedAt: UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *MagazineRepo) Update(ctx context.Context, req *organization_service.MagazineUpdate) (*organization_service.Magazine, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			magazines
		SET
			branch = :branch,
			name = :name,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":     req.Id,
		"branch": helper.NewNullString(req.Branch),
		"name":   helper.NewNullString(req.Name),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Magazine{
		Id:     req.Id,
		Name:   req.Name,
		Branch: req.Branch,
	}, nil
}

func (r *MagazineRepo) Delete(ctx context.Context, req *organization_service.MagazinePrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM magazines WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
