package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/pkg/helper"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *organization_service.BranchCreate) (*organization_service.Branch, error) {

	count, err := r.GetList(ctx, &organization_service.BranchGetListRequest{})
	if err != nil {
		return nil, err
	}

	var (
		id        = uuid.New().String()
		branch_id = helper.GenerateString(string(req.Name[0]), int(count.Count)+1)
		query     string
	)

	query = `
		INSERT INTO branches(id, branch_id, name, address, phone_number, updated_at)
		VALUES ($1, $2, $3, $4, $5,NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		branch_id,
		req.Name,
		req.Address,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          id,
		BranchId:    branch_id,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) GetByID(ctx context.Context, req *organization_service.BranchPrimaryKey) (*organization_service.Branch, error) {

	var (
		query string

		Id          sql.NullString
		BranchId    sql.NullString
		Name        sql.NullString
		Address     sql.NullString
		PhoneNumber sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	query = `
		SELECT
			id,
			branch_id,
			name,
			address,
			phone_number,
			created_at,
			updated_at
		FROM branches
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&BranchId,
		&Name,
		&Address,
		&PhoneNumber,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Branch{
		Id:          Id.String,
		BranchId:    BranchId.String,
		Name:        Name.String,
		Address:     Address.String,
		PhoneNumber: PhoneNumber.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *organization_service.BranchGetListRequest) (*organization_service.BranchGetListResponse, error) {

	var (
		resp   = &organization_service.BranchGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			name,
			address,
			phone_number,
			created_at,
			updated_at	
		FROM branches
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			BranchId    sql.NullString
			Name        sql.NullString
			Address     sql.NullString
			PhoneNumber sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Id,
			&BranchId,
			&Name,
			&Address,
			&PhoneNumber,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Branches = append(resp.Branches, &organization_service.Branch{
			Id:          Id.String,
			BranchId:    BranchId.String,
			Name:        Name.String,
			Address:     Address.String,
			PhoneNumber: PhoneNumber.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *organization_service.BranchUpdate) (*organization_service.Branch, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			branches
		SET
			address = :address,
			name = :name,
			phone_number = :phone_number,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"address":      helper.NewNullString(req.Address),
		"name":         helper.NewNullString(req.Name),
		"phone_number": helper.NewNullString(req.PhoneNumber),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Branch{
		Id:          req.Id,
		Name:        req.Name,
		Address:     req.Address,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM branches WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
