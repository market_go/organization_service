package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/market_go/organization_service/genproto/organization_service"
	"gitlab.com/market_go/organization_service/pkg/helper"
)

type ShipperRepo struct {
	db *pgxpool.Pool
}

func NewShipperRepo(db *pgxpool.Pool) *ShipperRepo {
	return &ShipperRepo{
		db: db,
	}
}

func (r *ShipperRepo) Create(ctx context.Context, req *organization_service.ShipperCreate) (*organization_service.Shipper, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO shippers(id, name, status, phone_number, updated_at)
		VALUES ($1, $2, $3, $4, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Name,
		req.Status,
		req.PhoneNumber,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Shipper{
		Id:          id,
		Status:      req.Status,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
	}, nil
}

func (r *ShipperRepo) GetByID(ctx context.Context, req *organization_service.ShipperPrimaryKey) (*organization_service.Shipper, error) {

	var (
		query string

		Id          sql.NullString
		Name        sql.NullString
		PhoneNumber sql.NullString
		Status      sql.NullString
		CreatedAt   sql.NullString
		UpdatedAt   sql.NullString
	)

	query = `
		SELECT
			id,
			name,
			phone_number,
			status,
			created_at,
			updated_at
		FROM shippers
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&Id,
		&Name,
		&PhoneNumber,
		&Status,
		&CreatedAt,
		&UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &organization_service.Shipper{
		Id:          Id.String,
		Name:        Name.String,
		PhoneNumber: PhoneNumber.String,
		Status:      Status.String,
		CreatedAt:   CreatedAt.String,
		UpdatedAt:   UpdatedAt.String,
	}, nil
}

func (r *ShipperRepo) GetList(ctx context.Context, req *organization_service.ShipperGetListRequest) (*organization_service.ShipperGetListResponse, error) {

	var (
		resp   = &organization_service.ShipperGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			phone_number,
			status,
			created_at,
			updated_at	
		FROM shippers
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			Id          sql.NullString
			Name        sql.NullString
			PhoneNumber sql.NullString
			Status      sql.NullString
			CreatedAt   sql.NullString
			UpdatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&Name,
			&PhoneNumber,
			&Status,
			&Name,
			&CreatedAt,
			&UpdatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Shipperes = append(resp.Shipperes, &organization_service.Shipper{
			Id:          Id.String,
			Name:        Name.String,
			PhoneNumber: PhoneNumber.String,
			Status:      Status.String,
			CreatedAt:   CreatedAt.String,
			UpdatedAt:   UpdatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ShipperRepo) Update(ctx context.Context, req *organization_service.ShipperUpdate) (*organization_service.Shipper, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			shippers
		SET
			name = :name,
			phone_number = :phone_number,
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"name":         req.Name,
		"phone_number": req.PhoneNumber,
		"status":       req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &organization_service.Shipper{
		Id:          req.Id,
		Name:        req.Name,
		PhoneNumber: req.PhoneNumber,
		Status:      req.Status,
	}, nil
}

func (r *ShipperRepo) Delete(ctx context.Context, req *organization_service.ShipperPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM shippers WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
